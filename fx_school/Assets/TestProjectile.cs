using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class TestProjectile : MonoBehaviour
{
    public Transform anchor;
    public float velocity = 3.0f;
    public float radius = 3.0f;
    public float height = 3.0f;

    private float previousFracTime = 0.0f;

    void Start()
    {
    }

    void Update()
    {
        float time = velocity * Time.time / (2 * radius);
        float fracTime = time - Mathf.Floor(time);

        int childCount = transform.childCount;
        for (int i = 0; i < childCount; ++i)
        {
            var child = transform.GetChild(i);
            var childFX = child.GetComponent<ParticleSystem>();
            if (childFX == null)
                continue;
            if (fracTime < previousFracTime && childFX.isPlaying)
            {
                childFX.Stop();
                childFX.Play();
            }
        }

        var myPosition = anchor.position + new Vector3(-radius + 2 * radius * fracTime, height, 0.0f);
        transform.position = myPosition;

        previousFracTime = fracTime;
    }
}
