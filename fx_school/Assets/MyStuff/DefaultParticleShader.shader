// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "VFX/Default Particle Shader"
{
	Properties
	{
		[Enum(UnityEngine.Rendering.CullMode)]_CullMode("Cull Mode", Float) = 2
		_MaskTexture("Mask Texture", 2D) = "white" {}
		[Toggle]_SwapUV("Swap UV", Float) = 0
		[Toggle]_GPUTimeUVOffset("GPU Time UV Offset", Float) = 0
		_Contrast("Contrast", Range( 0.25 , 16)) = 1
		_MasktoColor("Mask to Color", Range( 0 , 1)) = 0.5
		_Intensity("Intensity", Range( 1 , 100)) = 1
		_Opacity("Opacity", Range( 0 , 100)) = 1
		_DepthBlendDistance("Depth Blend Distance", Range( 0 , 10)) = 0.25
		_FadeAngle("Fade Angle", Range( 0 , 1)) = 0.25
		[Toggle]_InvertAngle("Invert Angle", Float) = 0
		[HideInInspector] _tex4coord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Transparent"  "Queue" = "Transparent+0" "IgnoreProjector" = "True" "IsEmissive" = "true"  "PreviewType"="Plane" }
		Cull [_CullMode]
		CGPROGRAM
		#include "UnityShaderVariables.cginc"
		#include "UnityCG.cginc"
		#pragma target 3.0
		#pragma multi_compile_instancing
		#pragma multi_compile_particles
		#pragma surface surf Unlit alpha:fade keepalpha noshadow noambient novertexlights nolightmap  nodynlightmap nodirlightmap nofog nometa noforwardadd 
		#undef TRANSFORM_TEX
		#define TRANSFORM_TEX(tex,name) float4(tex.xy * name##_ST.xy + name##_ST.zw, tex.z, tex.w)
		struct Input
		{
			float4 vertexColor : COLOR;
			float4 uv_tex4coord;
			float3 viewDir;
			float3 worldNormal;
			float4 screenPos;
		};

		uniform float _CullMode;
		uniform float _Intensity;
		uniform sampler2D _MaskTexture;
		SamplerState sampler_MaskTexture;
		uniform float _SwapUV;
		uniform float4 _MaskTexture_ST;
		uniform float _GPUTimeUVOffset;
		uniform float _Contrast;
		uniform float _Opacity;
		uniform float _FadeAngle;
		uniform float _InvertAngle;
		UNITY_DECLARE_DEPTH_TEXTURE( _CameraDepthTexture );
		uniform float4 _CameraDepthTexture_TexelSize;
		uniform float _DepthBlendDistance;
		uniform float _MasktoColor;

		inline half4 LightingUnlit( SurfaceOutput s, half3 lightDir, half atten )
		{
			return half4 ( 0, 0, 0, s.Alpha );
		}

		void surf( Input i , inout SurfaceOutput o )
		{
			float4 uvs4_MaskTexture = i.uv_tex4coord;
			uvs4_MaskTexture.xy = i.uv_tex4coord.xy * _MaskTexture_ST.xy + _MaskTexture_ST.zw;
			float2 temp_output_60_0 = (uvs4_MaskTexture).xy;
			float2 temp_output_61_0 = (uvs4_MaskTexture).zw;
			float2 lerpResult118 = lerp( temp_output_61_0 , ( temp_output_61_0 * _Time.y ) , _GPUTimeUVOffset);
			float temp_output_32_0 = pow( tex2D( _MaskTexture, ( (( _SwapUV )?( (temp_output_60_0).yx ):( temp_output_60_0 )) + lerpResult118 ) ).r , _Contrast );
			float3 ase_worldNormal = i.worldNormal;
			float dotResult50 = dot( i.viewDir , ase_worldNormal );
			float temp_output_102_0 = saturate( ( saturate( dotResult50 ) / _FadeAngle ) );
			float temp_output_86_0 = ( 1.0 - temp_output_102_0 );
			float lerpResult87 = lerp( ( temp_output_102_0 * temp_output_102_0 ) , ( temp_output_86_0 * temp_output_86_0 ) , _InvertAngle);
			float4 ase_screenPos = float4( i.screenPos.xyz , i.screenPos.w + 0.00000000001 );
			float4 ase_screenPosNorm = ase_screenPos / ase_screenPos.w;
			ase_screenPosNorm.z = ( UNITY_NEAR_CLIP_VALUE >= 0 ) ? ase_screenPosNorm.z : ase_screenPosNorm.z * 0.5 + 0.5;
			float screenDepth20 = LinearEyeDepth(SAMPLE_DEPTH_TEXTURE( _CameraDepthTexture, ase_screenPosNorm.xy ));
			float distanceDepth20 = saturate( abs( ( screenDepth20 - LinearEyeDepth( ase_screenPosNorm.z ) ) / ( _DepthBlendDistance ) ) );
			float temp_output_93_0 = ( 1.0 - distanceDepth20 );
			float FinAlpha106 = ( ( i.vertexColor.a * saturate( ( temp_output_32_0 * _Opacity ) ) ) * ( _FadeAngle == 0.0 ? ( 1.0 - _InvertAngle ) : lerpResult87 ) * ( 1.0 - ( temp_output_93_0 * temp_output_93_0 ) ) );
			float lerpResult78 = lerp( 1.0 , _Intensity , FinAlpha106);
			float lerpResult77 = lerp( _MasktoColor , 1.0 , temp_output_32_0);
			clip( FinAlpha106 - 0.001);
			o.Emission = ( lerpResult78 * (i.vertexColor).rgb * lerpResult77 );
			o.Alpha = FinAlpha106;
		}

		ENDCG
	}
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=18500
2560;0;1920;1019;3075.545;288.9738;1.902703;True;False
Node;AmplifyShaderEditor.TextureCoordinatesNode;56;-2230.029,369.4786;Inherit;False;0;3;4;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ComponentMaskNode;60;-1741.753,231.9803;Inherit;False;True;True;False;False;1;0;FLOAT4;0,0,0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.WorldNormalVector;91;-1057.849,918.8375;Inherit;False;False;1;0;FLOAT3;0,0,1;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.ViewDirInputsCoordNode;53;-1035.367,764.3889;Inherit;False;World;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.ComponentMaskNode;61;-1742.753,369.9802;Inherit;False;False;False;True;True;1;0;FLOAT4;0,0,0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleTimeNode;80;-1731.526,475.7906;Inherit;False;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;84;-1558.526,557.7906;Inherit;False;Property;_GPUTimeUVOffset;GPU Time UV Offset;3;1;[Toggle];Create;True;0;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;82;-1481.526,451.7906;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.DotProductOpNode;50;-758.5477,768.3065;Inherit;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SwizzleNode;64;-1511.753,292.9802;Inherit;False;FLOAT2;1;0;2;3;1;0;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SaturateNode;51;-576.5925,769.1262;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;118;-1280.954,376.7474;Inherit;False;3;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;79;-759.4764,675.3325;Inherit;False;Property;_FadeAngle;Fade Angle;9;0;Create;True;0;0;False;0;False;0.25;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.ToggleSwitchNode;62;-1329.753,230.9803;Inherit;False;Property;_SwapUV;Swap UV;2;0;Create;True;0;0;False;0;False;0;2;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleDivideOpNode;101;-355.9164,769.0471;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;57;-1060.753,350.9802;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;21;-669.0049,1073.175;Inherit;False;Property;_DepthBlendDistance;Depth Blend Distance;8;0;Create;True;0;0;False;0;False;0.25;10;0;10;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;33;-880.0159,523.3855;Inherit;False;Property;_Contrast;Contrast;4;0;Create;True;0;0;False;0;False;1;1;0.25;16;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;3;-879.4421,322.4923;Inherit;True;Property;_MaskTexture;Mask Texture;1;0;Create;True;0;0;False;0;False;-1;None;750b1bd7ba8bd28489650de6d0a95cc5;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SaturateNode;102;-155.9163,767.0471;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;86;46.86407,863.0174;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;39;-506.556,577.262;Inherit;False;Property;_Opacity;Opacity;7;0;Create;True;0;0;False;0;False;1;1;0;100;0;1;FLOAT;0
Node;AmplifyShaderEditor.DepthFade;20;-326.2738,1054.464;Inherit;False;True;True;True;2;1;FLOAT3;0,0,0;False;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.PowerNode;32;-490.2292,426.1821;Inherit;False;False;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;38;-129.6448,556.8203;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;100;268.0836,861.0471;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;88;245.784,954.2645;Inherit;False;Property;_InvertAngle;Invert Angle;10;1;[Toggle];Create;True;0;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;99;265.8057,764.7037;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;93;13.15125,1054.338;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;116;516.501,957.6843;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;87;516.3997,763.2645;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;92;265.1512,1028.337;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;37;61.28706,557.4837;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.VertexColorNode;11;0.6314011,282.0802;Inherit;False;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.Compare;111;774.7244,684.0903;Inherit;False;0;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;12;272.8326,532.4802;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;94;765.7445,1024.874;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;40;1036.816,531.9111;Inherit;False;3;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;106;1230.147,526.9993;Inherit;False;FinAlpha;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;5;472.2736,372.7853;Inherit;False;Property;_MasktoColor;Mask to Color;5;0;Create;True;0;0;False;0;False;0.5;0.5;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;107;279.551,239.8682;Inherit;False;106;FinAlpha;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;13;180.8198,160.4396;Inherit;False;Property;_Intensity;Intensity;6;0;Create;True;0;0;False;0;False;1;5;1;100;0;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;78;500.7026,141.8375;Inherit;False;3;0;FLOAT;1;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ComponentMaskNode;29;472.5489,276.4162;Inherit;False;True;True;True;False;1;0;COLOR;0,0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.LerpOp;77;805.9639,377.097;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;1;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;9;1041.863,258.3026;Inherit;False;3;3;0;FLOAT;0;False;1;FLOAT3;0,0,0;False;2;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.ClipNode;65;1518.953,387.6551;Inherit;False;3;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;2;FLOAT;0.001;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;85;2048.824,341.3549;Inherit;False;Property;_CullMode;Cull Mode;0;1;[Enum];Create;True;1;Option1;0;1;UnityEngine.Rendering.CullMode;True;0;False;2;2;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;31;1790.081,339.8488;Float;False;True;-1;2;ASEMaterialInspector;0;0;Unlit;VFX/Default Particle Shader;False;False;False;False;True;True;True;True;True;True;True;True;False;False;True;False;True;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Transparent;0.5;True;False;0;False;Transparent;;Transparent;All;14;all;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;False;2;5;False;-1;10;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;1;PreviewType=Plane;False;0;0;True;85;-1;0;False;-1;1;Pragma;multi_compile_particles;False;;Custom;0;0;False;0.1;False;-1;0;False;-1;False;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;60;0;56;0
WireConnection;61;0;56;0
WireConnection;82;0;61;0
WireConnection;82;1;80;0
WireConnection;50;0;53;0
WireConnection;50;1;91;0
WireConnection;64;0;60;0
WireConnection;51;0;50;0
WireConnection;118;0;61;0
WireConnection;118;1;82;0
WireConnection;118;2;84;0
WireConnection;62;0;60;0
WireConnection;62;1;64;0
WireConnection;101;0;51;0
WireConnection;101;1;79;0
WireConnection;57;0;62;0
WireConnection;57;1;118;0
WireConnection;3;1;57;0
WireConnection;102;0;101;0
WireConnection;86;0;102;0
WireConnection;20;0;21;0
WireConnection;32;0;3;1
WireConnection;32;1;33;0
WireConnection;38;0;32;0
WireConnection;38;1;39;0
WireConnection;100;0;86;0
WireConnection;100;1;86;0
WireConnection;99;0;102;0
WireConnection;99;1;102;0
WireConnection;93;0;20;0
WireConnection;116;0;88;0
WireConnection;87;0;99;0
WireConnection;87;1;100;0
WireConnection;87;2;88;0
WireConnection;92;0;93;0
WireConnection;92;1;93;0
WireConnection;37;0;38;0
WireConnection;111;0;79;0
WireConnection;111;2;116;0
WireConnection;111;3;87;0
WireConnection;12;0;11;4
WireConnection;12;1;37;0
WireConnection;94;0;92;0
WireConnection;40;0;12;0
WireConnection;40;1;111;0
WireConnection;40;2;94;0
WireConnection;106;0;40;0
WireConnection;78;1;13;0
WireConnection;78;2;107;0
WireConnection;29;0;11;0
WireConnection;77;0;5;0
WireConnection;77;2;32;0
WireConnection;9;0;78;0
WireConnection;9;1;29;0
WireConnection;9;2;77;0
WireConnection;65;0;9;0
WireConnection;65;1;106;0
WireConnection;31;2;65;0
WireConnection;31;9;106;0
ASEEND*/
//CHKSM=D3BA41B2BBFA59AC028B89C719B33B3293BE84A3